FROM python:3.8
COPY gunicorn_configuration.py /etc/
COPY setup.py /id/
COPY identity_service/* /id/identity_service/
RUN pip install /id/
RUN rm --recursive --force /id/
CMD ["gunicorn", "--config=/etc/gunicorn_configuration.py", "identity_service.api:application"]
EXPOSE 8000/tcp