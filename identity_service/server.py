from keycloak import KeycloakAdmin

from identity_service import configuration


class NotFound(Exception):
    pass


class Keycloak:
    def __init__(self):
        server = configuration.keycloak_server
        self.disabled_on_delete = server["disabled_on_delete"]
        self.auto_verify_email = server["auto_verify_email"]
        self.keycloak_admin = KeycloakAdmin(
            server_url=server["url"],
            client_id=server["client_id"],
            client_secret_key=server["client_secret"],
            realm_name=server["realm"],
            verify=server.get("verify", True),
        )

    def search(self, username):
        user = self.keycloak_admin.get_user(self.get_user_id(username))
        cleaned_user = {
            "username": user["username"],
            "account_locked": not user["enabled"],
            "area_access": [],
            "can_reset_password_and_unlock_account": True,
        }
        return cleaned_user

    def create(self, username, email):
        description = ""
        user_id = None
        try:
            # Try and find the username. Attempt to create it if it doesn't exist.
            user_id = self.get_user_id(username)
            description = "User found\n"
        except NotFound:
            self.keycloak_admin.create_user(
                {"email": email, "username": username, "enabled": True, "emailVerified": self.auto_verify_email}
            )
            description += f"Created user: {username}\n"

            self.reset_password(username)
            description += "Default password set\n"

        # Update email if user was found.
        if user_id and email:
            self.keycloak_admin.update_user(
                user_id=user_id, payload={"email": email, "emailVerified": self.auto_verify_email}
            )
            description += f"Email updated for {username}"

        return True, description

    def delete(self, username):
        if self.disabled_on_delete:
            user_id = self.get_user_id(username)
            self.keycloak_admin.update_user(user_id=user_id, payload={"enabled": False})
            description = f"Disabled user: {username}"
        else:
            self.keycloak_admin.delete_user(self.get_user_id(username))
            description = f"Deleted user: {username}"
        return True, description

    def unlock_account(self, username):
        user_id = self.get_user_id(username)
        self.keycloak_admin.update_user(user_id=user_id, payload={"enabled": True})
        description = f"Account unlocked set for {username}"
        return True, description

    def reset_password(self, username):
        user_id = self.get_user_id(username)
        self.keycloak_admin.set_user_password(user_id, configuration.default_password(), temporary=True)
        description = f"Default password set for {username}"
        return True, description

    def get_user_id(self, username):
        users = self.keycloak_admin.get_users(query={"username": username})
        user_id = next((user["id"] for user in users if user["username"] == username), None)
        if not user_id:
            raise NotFound(f"Username {username} was not found")
        return user_id
